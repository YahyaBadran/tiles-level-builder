# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

The source code of the Tiles Level Editor. To get an idea see the following video :

[https://youtu.be/B5b2wTN1G7s](Youtube : Unity Level Builder)




### Set up ###

1) Clone this repository onto the Editor Folder of your Unity Project.
2) A new Menue will show up with the Name "Tools" that contains the Option "LevelBuilder", it will show the Editor Window, and let you use the Editor.

### License : MIT ###